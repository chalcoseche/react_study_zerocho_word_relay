^
^React Hooks 사용하기
^

React가 추천하는방법은 class를 사용X ( 지금까지의 설명은 class였음)
그치만, 대부분의 코드는 class라 보고 알수있을정도는 공부해야함

class Gugudan extends React.Component 가아닌,

const Gugudan = () => { // 함수 컴포넌트
return
}

Hooks 란, use가 붙어있는것들 useState 등.

함수 컴퍼넌트에서 state를 사용ㅇ하는걸 React Hooks 라 함,

const [first,setFirst] = React.useState
const [second,setSecond] = React.useState

hooks방식에 익숙해져야함

& useState('') 에서 초기값을 입력함.
& useState 선언들은 꼭 함수 안에 작성 해야함

return <>{first}{second}</>

이벤트리스너와 ref부분에서 class 컴퍼넌트와 다르다.

onChangeInput = (e) =>{ //똑같이 만든다.
setValue(e.target.value) //약속이니까 그냥 이렇게 쓰면 됨
onSubmitForm =(e)=>{
e.preventDefault();
setstate ~~~~ // 여기에 set부분을 변경 작성 하면 된다.
}

}

ref 사용법

const inputRef =React.useRef(초기값 넣어두됨);

<tag ref={inputref}>
 작성후에
inputRef.current.input.focus(); 로작성한다.

두가지 모두 공부 하기

0128

^
^ 2// Class와 Hooks 비교하기
^
Hooks는 this.state 사용X
ref는 ref.current.xxx() 를 사용
setState부분과 ref방법이 서로 다르다

(코드는 Hooks부분이 좀더 짧다)

hooks는 클래스에비해 조금더 연산이 느릴수 있다.( 큰 차이아님 )
class는 render()부분만 리렌더링 되고, Hooks는 전체가 리렌더링됨
=Hooks는 useCallback으로 막고,값은 useMemo로 캐싱함 ( 추후 배울것)

리액트가 알아서 setState들을 모아서 한번만 렌더링 해줌.

- 리액트에서는 클래스 못씀 ClassName 을 사용
  For도 for문과 헷갈리기 때문에 htmlFor로 사용

setState( { 이런식으로 객체 작성 가능}) (함수도객체라 가능)

state를 객체로 만들면 ? 가능은하지만 setState이용시 어렵다
Class에서는 변경부분만 setSTate에 작성해도 작성됐지만
Hooks에서는 생략해버리며 아예 사라져버림.
그래서 Hooks 는 setState를 쪼개서 사용함 (합쳐서 사용하면 위에같이 생략되는 문제때매 작성하기 귀찮아짐(길어짐))

옛날 state를 활용하고싶은경우
setState ((prevResult)=>{
return '정답' + value
})
매개변수를 이용해서 예전 결과물을 가져올 수 있다.

^
^ 2// 웹팩 설치하기
^

개발을 실제로 할 때 컴포넌트가 단일이 아닐것, 수십가지 수백가지가 될 수도 있음

컴포넌트마다 스크립트를 하나씩 만든다고 하면, 20개정도 = 코딩유지보수가 굉장히 어려워짐

수십 수백 수천 수만개의 컴포넌트를 하나의 js로 합쳐주는게 웹팩

react react-dom webpack 설치하기 (create-react-app 이해하기)

.js와 .jsx로 구별하여 명시성 챙기기

원하는 프로젝트 폴더 npm init으로 author 내이름, license MIT후 yes입력

npm install react react-dom

npm i -D webpack webpack-cli

// -D 는 개발환경에서만 사용하기위해 설치하겠다는 뜻 (webpack은 배포용이 아님)

프로젝트에서 npm i 로설치하게되면 package.json 에 추가됨
"dependencies"
"devDependencies"

clients.jsx 로 react와 react-dom 을 불러오기,
webpack.config.js 에 module.experts를 작성

이러한방식을 자동화 해주는게 create-react-app 터미널 방식임.

^
^ 2// 모듈 시스템과 웹팩 설정
^

jsx파일로 분리해줄떄마다
const React =require('react');로 react를 불러와준다
const {Component} = React;

moduelx.exports = WordRelay;

html 에서 인식하는건 코드가 2만개여도 html 자체에선 app.js 하나만 인식 한다.
= webpack 을이용해서 하나로 합쳐준다 input(합쳐지는 jsx,js) / output(app.js)

wepack config js 를 설정해준다. input output

const path =require('path'); < 외우기
path: path.join(\_\_dirname, 'dist'), < 외우기

path는 노드에서 경로를 잡아줌
\_\_dirname현재 경로, 'dist' 현재폴더에있는 dist 절대경로를 바꿔줌

entry {
app:[현재 작성된 jsx,js]
}

resolve : 웹팩이 알아서 확장자를 찾아준다.

^
^2-5 웹팩으로 빌드하기
^

명령어없다고뜨면 npx사용하거나, package.json 에 추가하기

에러뜨면 babel설치하기

^
^ 2-6 구구단으로 웹팩으로 빌드하기
^

웹팩 복습 (만들어둔 구구단 웹팩으로 빌드하기 )

1.  npm init
    package name : 프로젝트 명
    AUTHOR : 제작자
    license : MIT

2.  npm i react react-dom
3.  npm i -D webpack webpack-cli

4.  npm i babel-loader @babel/core @babel/preset-env @babel/preset-react

5.  webpack.config.js 설정하기
    const path =require ('path');
    module.exports ={
    (순서대로 작성)
    mode: 'development',
    devtool :'eval', // hidden-source-map
    resolve:{
    extensions:['.jsx','.js'],
    }
    entry:{
    app:'./client',

    },
    module:{
    rules:[{
    test:\_/\.jsx?$/,
    loader:'babel-loader',
    options:{
    presets:['@babel/preset-env','@babel/preset-react'], (문제가 생겼을때 추가적으로 그때마다 설치하는것이 빌드 최적화에 좋다.)
    plugins:[],
    }
    }],

    }
    output:{
    filename:'app.js',
    path:path.join(\_\_dirname,'dist'),
    },
    };
    (프리셋 거의고정)

6.  package.json 설정하기

    "scripts":{
    "dev":"webpack",
    }

7.  만든 구구단 html 을 jsx로 붙여넣기 ( 영상 과정 // 직접만들어보기)

8.  client.jsx
    const React = require('react');
    const ReactDOM = require('react-dom');

    ReactDOM.render(<GuGudan />, documnet.querySelector('#root'));
    const GuGuDan = require('./GuGuDan');

9.  React 모듈이 필요한 jsx파일에
    const React = require('react');
    const {useSTate,useRef}= React;

10. React.Fragment 는 바벨이 처리했으니 안심하라구!

11. npx webpack, npm run dev 택 일

^
^ @babel/preset-env 와 plugins
^

제로초 왈 : 웹팩은 설정의 연속

entry 에 들어간 jsx 를 module-loader-babel-loader 적용하고 바벨에대한 옵션은 presets 에있다.

preset 안에 babel-loader가 또 있을수있다.(preset 1, module-loader 1 )

플러그인의 모음 = 프리셋

preset-env 에 여러개의 설정을 하고싶다.
= 프리셋을 배열로 바꾼뒤, 두번째 요소에 설정을 적는다.

['@babel/preset-env',{targets:{
browsers:['last 2 chrome versions', '>5% in KR'], (브라우저 호환 적용 기기 ) 원하는 브라우저 라인업에 설정가능 중요
},}],
debug:true, (개발용)

plugins: [], ( 추가적으로 무언가 하고싶으면 여기에 작성)
new webpack.LoaderOptionsPlugin({debug:true}),

browserslist : 브라우저 리스트 나오기

^
^ 끝말잇기 Class만들기
^

(클래스 함수)
바뀌는 부분은 state 다 !

class WordRelay extends Component {
state ={
word :'제로초',
value:'',
result:'',
};
}

onSubmitForm =(e) =>{
e.preventDefault();
if(this.state.word[this.state.word.length-1] === this.state.value[0]) [끝말잇기의 로직 부분] {
this.setState({
result:'딩동댕',
word:value,
value:'',
});
this.input.focus(); (Ref를 이용해 DOM 에 접근함 )
} else {
this.setState({
result:'땡',
value:'',
});
this.input.focus();
}

};

onChangeInput = (e) =>{
this.setState({value:e.target.value});

}
input;
onRefInput =(c)=>{
this.input =c;

}
이렇게 빼줘야 state 바뀔때마다 렌더가 재실행되는데, 재실행되는것이아니라 참조하는것이기때매 밖으로 뺴는게 효율적이다
render() {
return(
<>

<div>{this.state.word}</div>
  <form onSubmit={this.onSubmitForm}>
<input ref={this.onRefInput} value ={this.state.value} onChange={this.onChangeInput />
<button>입력!</button>
  </form>
  <div>{this.state.result}</div>
  </>
  )
}

직접만든 함수들은 화살표 함수로 만든다

value 와 onChange는 세트 // 그게아니면 defaultValue

빌드를 자동으로해주는 설정

^
^ 웹팩 데브 서버와 핫 리로딩
^

^미래^에서 온 제로초가 설명해줌 :

웹팩 핫리로딩 세팅에서 달라진게 많다

핫 리로딩하는 두 패키지

npm i react-refresh 와 @pmmmwh/react-refresh-webpack-plugin -D

npm i -D webpack-dev-server 개발용 개인 서버 패키지

"scripts" :{
"dev":"webpack serve --env development" 로 변경됨
}

webpack.config.js 와서
다운 받은 패키지를 불러오기 한다.
require 로 불러오고
plugin 에 변수명 붙여넣기.

devServer:{} 설정은 아웃풋 다음줄에 적어준다. ( 개발 편의를 위해 사용)

devServer :{
publicPath:'/dist/', (가상의 경로 express.static 과 비슷함)
hot:true
}

^더^ 미래에서 온 제로초가 설명해줌 :

한가지 변경된 점 ;

devServer :{
devMiddleware:{publicPath:'/dist/',} ( 데브서버는 메모리의 생성을한다.)
static:{directory:path.resolve(\_\_dirname)}; (실제로 존재하는 정적 폴더들의 경로)
hot:true
}

^
^끝말잇기 Hooks로 전환하기
^

const {useState,useRef} = React;

ref 는 const inputRef =userRef(null);

메서드들은 더이상 클래스가 아니라, 선언 해줘야한다.
this < 더이상 안쓰게되서 지워준다.
setState도 사용안함.

inputRef.current.focus() ref 는 current 를 사용해야함.

HMR : hot module reload ;
어떤게 바껴서 업데이트 된건지 알려줌.

^
^컨트롤드 인풋 vs 언컨트롤드 인풋
^

&미래에서온 제로초 :

인풋에는 두가지 사용법이있다

uncontrolled input 이란 value와 onChange가 없다.

리액트는 controlled input을 권장한다.

간단하다 : 언컨트롤드 = 만드려는게 간단하다하면 언컨트롤드,

뭔가 구현이 복잡하다 : 컨트롤드를 사용하는게 좋다

대부분 복잡합 앱을 구현하기 떄문에 컨트롤드를 사용하면된다.

& value가 form 에있는 onSubmit 안에서만 쓰이는경우는 uncontrolled 해도됨.

form안에 순서대로 input이 들어있다면 [0]부터 시작하는 인덱스가 붙음.

1. 밑에 빨간줄이 떠야하는것 = dynamic inputs 는 controlled일때만 작동
2. 버튼 자체를 못누르게하는것 = disabling submit button 는 controlled일때만 작동

3. uncontrolled는 다수의 input을
   e.target.children.word[0]
   e.target[0]
   e.target[1]
   e.target[2]
   등 사용할 수 있는데

controlled 는 state에서 받을수 있기 떄문에 조합이 가능하다.

= controlled 사용만해두 문제 없다.

초기값은 value가아닌 ,defaultValue (uncontrolle에 허용된건 이것뿐)
